package com.conge.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

 

@Entity 
@Table(name = "departement")
public class Departement implements Serializable {
    @Id
    @GeneratedValue
	private long idDepartement;
    private String numDep;
    private String nomDep;
    
    @OneToMany(mappedBy = "departement")
    private List<Enseignant> enseignants;
	public long getIdDepartement() {
		return idDepartement;
	}
	public void setIdDepartement(long idDepartement) {
		this.idDepartement = idDepartement;
	}
	public String getNumDep() {
		return numDep;
	}
	public void setNumDep(String numDep) {
		this.numDep = numDep;
	}
	public String getNomDep() {
		return nomDep;
	}
	public void setNomDep(String nomDep) {
		this.nomDep = nomDep;
	}
	public List<Enseignant> getEnseignants() {
		return enseignants;
	}
	public void setEnseignants(List<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}
   
    
    
}
 