package com.conge.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table(name = "grade")
public class Grade implements Serializable {
    @Id
    @GeneratedValue
	private long idGrade;
    private String codeGrade;
    private String intituleGrade;
    
    @OneToMany(mappedBy = "grade")
    private List<Enseignant> enseignants;


	public long getIdGrade() {
		return idGrade;
	}


	public void setIdGrade(long idGrade) {
		this.idGrade = idGrade;
	}


	public String getCodeGrade() {
		return codeGrade;
	}


	public void setCodeGrade(String codeGrade) {
		this.codeGrade = codeGrade;
	}


	public String getIntituleGrade() {
		return intituleGrade;
	}


	public void setIntituleGrade(String intituleGrade) {
		this.intituleGrade = intituleGrade;
	}


	public List<Enseignant> getEnseignants() {
		return enseignants;
	}


	public void setEnseignants(List<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}
    
    
}
