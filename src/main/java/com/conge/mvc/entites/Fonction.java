package com.conge.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table(name = "fonction")
public class Fonction implements Serializable {
    @Id
    @GeneratedValue
	private long idFonction;
    private String codeFonct;
    private String intitule_Fonct;
    
    @OneToMany(mappedBy = "fonction")
    private List<Enseignant> enseignants;


	public long getIdFonction() {
		return idFonction;
	}


	public void setIdFonction(long idFonction) {
		this.idFonction = idFonction;
	}


	public String getCodeFonct() {
		return codeFonct;
	}


	public void setCodeFonct(String codeFonct) {
		this.codeFonct = codeFonct;
	}


	public String getIntitule_Fonct() {
		return intitule_Fonct;
	}


	public void setIntitule_Fonct(String intitule_Fonct) {
		this.intitule_Fonct = intitule_Fonct;
	}


	public List<Enseignant> getEnseignants() {
		return enseignants;
	}


	public void setEnseignants(List<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}
    
    
    
}

