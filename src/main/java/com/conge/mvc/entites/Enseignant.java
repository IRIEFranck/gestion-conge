package com.conge.mvc.entites;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity 
@Table(name = "enseignant")
public class Enseignant implements Serializable {
    @Id
    @GeneratedValue
	private long idEnseignant;
    private int codeEnsei;	
	private String nom;
	private String prenom;
	private String sexe;
	
	
	private Date date_naissance;
	private Date lieu_naissance;
	private String nationalite;
	private String situation;
	private int nombre_enfant;
	private String adresse;
	private String telephone;
	private String email;
	private String photo;
	private Date date_recrutement;
	private String lieu_recrutement;
	
	@ManyToOne
	@JoinColumn(name= "idDepartement")
	private Departement departement;
	@ManyToOne
	@JoinColumn(name= "idFonction")
	private Fonction fonction;
	@ManyToOne
	@JoinColumn(name= "idGrade")
	private Grade grade;
	@ManyToOne
	@JoinColumn(name= "idDiplome")
	private Diplome diplome;
	public long getIdEnseignant() {
		return idEnseignant;
	}
	public void setIdEnseignant(long idEnseignant) {
		this.idEnseignant = idEnseignant;
	}
	public int getCodeEnsei() {
		return codeEnsei;
	}
	public void setCodeEnsei(int codeEnsei) {
		this.codeEnsei = codeEnsei;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	public Date getLieu_naissance() {
		return lieu_naissance;
	}
	public void setLieu_naissance(Date lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public String getSituation() {
		return situation;
	}
	public void setSituation(String situation) {
		this.situation = situation;
	}
	public int getNombre_enfant() {
		return nombre_enfant;
	}
	public void setNombre_enfant(int nombre_enfant) {
		this.nombre_enfant = nombre_enfant;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Date getDate_recrutement() {
		return date_recrutement;
	}
	public void setDate_recrutement(Date date_recrutement) {
		this.date_recrutement = date_recrutement;
	}
	public String getLieu_recrutement() {
		return lieu_recrutement;
	}
	public void setLieu_recrutement(String lieu_recrutement) {
		this.lieu_recrutement = lieu_recrutement;
	}
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public Fonction getFonction() {
		return fonction;
	}
	public void setFonction(Fonction fonction) {
		this.fonction = fonction;
	}
	public Grade getGrade() {
		return grade;
	}
	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	public Diplome getDiplome() {
		return diplome;
	}
	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}
	
	
	
	
	
	
	
	
        	
	
}
