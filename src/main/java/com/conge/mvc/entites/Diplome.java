package com.conge.mvc.entites;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "diplome")
public class Diplome implements Serializable {
     @Id 
     @GeneratedValue
     
	private long idDiplome;
    private String codeDiplome;
    private String intituleDiplome;
    private Date date_obtention;
    private String lieu_Obtention;
    private String Specialite;
    
    @OneToMany(mappedBy = "diplome")
    private List<Enseignant> enseignants;

	public long getIdDiplome() {
		return idDiplome;
	}

	public void setIdDiplome(long idDiplome) {
		this.idDiplome = idDiplome;
	}

	public String getCodeDiplome() {
		return codeDiplome;
	}

	public void setCodeDiplome(String codeDiplome) {
		this.codeDiplome = codeDiplome;
	}

	public String getIntituleDiplome() {
		return intituleDiplome;
	}

	public void setIntituleDiplome(String intituleDiplome) {
		this.intituleDiplome = intituleDiplome;
	}

	public Date getDate_obtention() {
		return date_obtention;
	}

	public void setDate_obtention(Date date_obtention) {
		this.date_obtention = date_obtention;
	}

	public String getLieu_Obtention() {
		return lieu_Obtention;
	}

	public void setLieu_Obtention(String lieu_Obtention) {
		this.lieu_Obtention = lieu_Obtention;
	}

	public String getSpecialite() {
		return Specialite;
	}

	public void setSpecialite(String specialite) {
		Specialite = specialite;
	}

	public List<Enseignant> getEnseignants() {
		return enseignants;
	}

	public void setEnseignants(List<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}
    
    
     
}
