package com.conge.mvc.dao;

import com.conge.mvc.entites.Diplome;

public interface IDiplomeDao extends IGenericDao<Diplome> {

}
