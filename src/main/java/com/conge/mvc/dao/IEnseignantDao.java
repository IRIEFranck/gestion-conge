package com.conge.mvc.dao;

import com.conge.mvc.entites.Enseignant;

public interface IEnseignantDao extends IGenericDao<Enseignant> {

}
