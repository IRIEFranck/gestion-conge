package com.conge.mvc.dao;

import com.conge.mvc.entites.Departement;


public interface IDepartementDao  extends IGenericDao<Departement> {

}
