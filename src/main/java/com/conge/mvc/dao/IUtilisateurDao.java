package com.conge.mvc.dao;


import com.conge.mvc.entites.Utilisateur;

public interface IUtilisateurDao  extends IGenericDao<Utilisateur> {

}
